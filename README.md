# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository is for Weather Forecast application.

### How do I get set up? ###

The solution contains two applications
a) Web API - This is the web service which consumes openweathermap api and provides 5 day weather forecast data for a given location.
Example:
http://localhost:58900/weather/london

b). Web App - This is an mvc app integrated with angular. Index.html is the default page.
Enter the location and press the button to retrieve and display the data. If it doesn't find any data, then the page doesn't display any error. 

Steps to run the application:
.The web api uses city.list.json (list of Location names and respective IDs) file for lookup. (ie finds the ID based on location and then uses this ID to call openweathermap api)
Unzip the city.list.zip and copy the city.list.json file to any folder and configure the name in the web.config file.
By default it is configured to use "c:\temp\city.list.json"

.I have not included the node components used by the angular application. 
So please go to weather.webapp folder using nodejs command prompt and install the components using "npm install" 

. Both weather.webapi and weather.webapp have been set as startup projects. 

. webapi uses 58900 port. If this is changed, then the url should be updated in weather.service.ts file in weather.webapp/app folder.

. Run the application from visual studio. This should start both webapi and webapp applications.

If you find any issues in building the solution, then please don't hesitate to contact me.

### Contribution guidelines ###

### Who do I talk to? ###
diwansanjay@gmail.com
