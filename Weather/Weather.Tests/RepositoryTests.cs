﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Weather.Repositories;
using FakeItEasy;
using Weather.Entities;
using System.Collections.Generic;

namespace Weather.Tests
{
    [TestClass]
    public class RepositoryTests
    {

        IWeatherRepository _weatherRepository;
        ILookupRepository _lookupRepository;

        [TestInitialize]
        public void Initialize()
        {
            _weatherRepository = A.Fake<IWeatherRepository>();
            _lookupRepository = A.Fake<ILookupRepository>();

            A.CallTo(() => _lookupRepository.GetCity(string.Empty)).Returns(new CityItem { country = "GB", name = "London", _id = "123" });
        }

        [TestMethod]
        public void when_city_lookup_is_queried_by_name_then_repository_should_return_city()
        {
            List<CityItem> cityItemList = new List<CityItem>();
            cityItemList.Add(new CityItem { country = "GB", name = "London", _id = "1" });
            cityItemList.Add(new CityItem { country = "GB", name = "Cardiff", _id = "2" });
            cityItemList.Add(new CityItem { country = "GB", name = "Birmingham", _id = "3" });

            ILookupRepository lookupRepository = new DummyLookupRepository(cityItemList);

            var city = lookupRepository.GetCity("Cardiff");

            Assert.IsNotNull(city);
            Assert.AreEqual("2", city._id);
        }

    }
}