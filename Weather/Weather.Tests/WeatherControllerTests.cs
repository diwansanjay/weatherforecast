﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Weather.Repositories;
using FakeItEasy;
using Weather.Api.Controllers;
using Weather.Configuration;
using System.Web.Http;
using System.Net;

namespace Weather.Tests
{
    [TestClass]
    public class WeatherControllerTests
    {
        IWeatherRepository _weatherRepository;
        IConfigurationManager _configuration;

        [TestInitialize]
        public void Initialize()
        {
            _weatherRepository = A.Fake<IWeatherRepository>();
            _configuration = A.Fake<IConfigurationManager>();
        }

        [TestMethod]
        public void when_weather_is_searched_for_a_city_then_controller_should_use_repository_to_return_weather_forecast_for_that_city()
        {
            List<CityItem> cityItemList = new List<CityItem>();
            cityItemList.Add(new CityItem { country = "GB", name = "London", _id = "1" });
            cityItemList.Add(new CityItem { country = "GB", name = "Cardiff", _id = "2" });
            cityItemList.Add(new CityItem { country = "GB", name = "Birmingham", _id = "3" });

            var dummyWeatherForecast = new WeatherForecast();
            dummyWeatherForecast.id = "1";
            dummyWeatherForecast.name = "London";

            ILookupRepository lookupRepository = new DummyLookupRepository(cityItemList);
            A.CallTo(() => _weatherRepository.GetWeatherForecast("London")).Returns(dummyWeatherForecast);

            WeatherController weatherController = new WeatherController(_configuration, lookupRepository, _weatherRepository);
            var weatherForecast = weatherController.Get("London");
            Assert.AreEqual("London", weatherForecast.name);
            Assert.AreEqual("1", weatherForecast.id);

        }

        [TestMethod]
        public void when_weather_forecast_is_not_found_then_weather_controller_should_return_null_values_in_weather_forecast()
        {
            List<CityItem> cityItemList = new List<CityItem>();
            cityItemList.Add(new CityItem { country = "GB", name = "London", _id = "1" });
            cityItemList.Add(new CityItem { country = "GB", name = "Cardiff", _id = "2" });
            cityItemList.Add(new CityItem { country = "GB", name = "Birmingham", _id = "3" });

            var dummyWeatherForecast = new WeatherForecast();
            dummyWeatherForecast.id = "1";
            dummyWeatherForecast.name = "London";

            ILookupRepository lookupRepository = new DummyLookupRepository(cityItemList);
            A.CallTo(() => _weatherRepository.GetWeatherForecast("London")).Returns(dummyWeatherForecast);

            WeatherController weatherController = new WeatherController(_configuration, lookupRepository, _weatherRepository);
            var weatherForecast = weatherController.Get("Glasgow");

            Assert.IsNull(weatherForecast.id);           
        }
    }
}

