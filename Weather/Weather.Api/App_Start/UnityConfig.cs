using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using Weather.Repositories;
using Weather.Configuration;

namespace Weather.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IConfigurationManager, FileBasedConfigurationManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<ILookupRepository, LookupRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<IWeatherRepository, WeatherRepository>(new PerThreadLifetimeManager());

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}