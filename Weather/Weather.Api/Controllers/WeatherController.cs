﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Weather.Entities;
using Weather.Repositories;
using Weather.Configuration;

namespace Weather.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [RoutePrefix("Weather")]
    public class WeatherController : ApiController
    {
        private IConfigurationManager _configuration;
        private ILookupRepository _lookupRepository;
        private IWeatherRepository _weatherRepository;

        public WeatherController(IConfigurationManager configuration, ILookupRepository lookupRepository, IWeatherRepository weatherRepository)
        {
            _configuration = configuration;
            _lookupRepository = lookupRepository;
            _weatherRepository = weatherRepository;
        }

        [Route("{cityName}")]
        public WeatherForecast Get(string cityName)
        {
            WeatherForecast weatherForecast =null;

            try
            {
                weatherForecast = _weatherRepository.GetWeatherForecast(cityName);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message));
            }

            return weatherForecast;
        }
    }
}
