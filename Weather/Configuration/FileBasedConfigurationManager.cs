﻿using Weather.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Weather.Configuration
{
    public class FileBasedConfigurationManager : IConfigurationManager
    {
        public NameValueCollection AppSettings
        {
            get
            {   
                return ConfigurationManager.AppSettings;
            }
        }

        public string ConnectionStrings(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ToString();
        }
    }
}
