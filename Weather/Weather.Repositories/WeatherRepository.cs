﻿using Weather.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.Entities;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;

namespace Weather.Repositories
{
    public class WeatherRepository : IWeatherRepository
    {
        private readonly IConfigurationManager _configuration;
        private readonly ILookupRepository _lookupRepository;

        public WeatherRepository(IConfigurationManager configuration, ILookupRepository lookupRepository)
        {
            _configuration = configuration;
            _lookupRepository = lookupRepository;
        }

        public WeatherForecast GetWeatherForecast(string cityName)
        {
            WeatherData weatherData=null;
            WeatherForecast weatherForecast = null;

            var city = _lookupRepository.GetCity(cityName);

            if (city!=null)
            {
                var url = string.Format("{0}&appid={1}&id={2}", _configuration.AppSettings["OpenWeatherMap.ForecastUrl"], _configuration.AppSettings["AppId"], city._id);

                var response = HttpClientHelper.CallAsync(url);
                weatherData = response.Result.Content.ReadAsAsync<WeatherData>().Result;

                weatherForecast = GetFromWeatherData(weatherData, city);
            }

            return weatherForecast;
        }

        private WeatherForecast GetFromWeatherData(WeatherData weatherData, CityItem city)
        {
            WeatherForecast weatherForecast = null;

            if (weatherData != null)
            {
                weatherForecast = new WeatherForecast();
                weatherForecast.id = city._id;
                weatherForecast.name = city.name;

                weatherForecast.temparature = new List<Temparature>();
                //For simplicity, just taking the first item for the day i.e. at 00:00:00 time
                for (var i = 0; i < weatherData.list.Count; i = i + 8)
                {
                    var forecast = weatherData.list[i];

                    var temparature = new Temparature();
                    temparature.dt = Convert.ToDateTime(forecast.dt_txt).ToShortDateString();

                    temparature.description = forecast.weather[0].description;
                    
                    temparature.temp = decimal.Round(Convert.ToDecimal(forecast.main.temp),0);
                    temparature.minTemp = decimal.Round(Convert.ToDecimal(forecast.main.temp_min), 0);
                    temparature.maxTemp = decimal.Round(Convert.ToDecimal(forecast.main.temp_max), 0);
                    temparature.tempImageUrl = string.Format("{0}{1}.png", _configuration.AppSettings["WeatherIconUrl"], forecast.weather[0].icon);
                    weatherForecast.temparature.Add(temparature);
                }
            }
            return weatherForecast;
        }
    }
}
