﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.Entities;
using Weather.Configuration;

namespace Weather.Repositories
{
    public class LookupRepository : ILookupRepository
    {
        private List<CityItem> _cities = new List<CityItem>();
        private IConfigurationManager _configuration;

        public LookupRepository(IConfigurationManager configuration)
        {
            _configuration = configuration;
        }

        public List<CityItem> Cities
        {
            get
            {
                //Load once
                if (!_cities.Any())
                {
                    var fileName = _configuration.AppSettings["CityList"];
                    using (var streamReader = new StreamReader(fileName))
                    {
                        var data = streamReader.ReadToEnd();
                        var cities = JsonConvert.DeserializeObject<Cities>(data);
                        _cities = cities.cities;
                    }
                }

                return _cities;
            }
        }

        public CityItem GetCity(string cityName)
        {
            var city = Cities.Where(c => c.name.Equals(cityName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

            return city;
        }
        
    }
}
