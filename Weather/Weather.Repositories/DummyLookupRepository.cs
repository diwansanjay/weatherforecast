﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.Entities;

namespace Weather.Repositories
{
    public class DummyLookupRepository : ILookupRepository
    {
        private List<CityItem> _cities = new List<CityItem>();

        public DummyLookupRepository(List<CityItem> cities)
        {
            _cities = cities;
        }

        public List<CityItem> Cities
        {
            get
            {
                return _cities;
            }
        }

        public CityItem GetCity(string cityName)
        {
            var city = Cities.Where(c => c.name.Equals(cityName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

            return city;
        }
        
    }
}
