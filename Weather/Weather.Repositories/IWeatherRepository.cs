﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.Entities;

namespace Weather.Repositories
{
    public interface IWeatherRepository
    {
        WeatherForecast GetWeatherForecast(string cityName);
    }
}
