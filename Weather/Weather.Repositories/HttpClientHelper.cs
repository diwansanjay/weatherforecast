﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Repositories
{
    public static class HttpClientHelper
    {
        public static Task<HttpResponseMessage> CallAsync(string url)
        {
            var t = Task.Run(() =>
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = httpClient.GetAsync(url).Result;

                    return response;
                }

            });

            return t;
        }

    }
}
