﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weather.Entities;

namespace Weather.Repositories
{
    public interface ILookupRepository
    {
        List<CityItem> Cities { get; }
        CityItem GetCity(string cityName);

    }
}
