﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Entities
{
    public class Cities
    {
        public List<CityItem> cities { get; set; }
    }

    public class CityItem
    {
        public string _id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
    }
}
