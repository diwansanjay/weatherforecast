﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Entities
{
    public class WeatherData
    {
        public string cod { get; set; }
        public string message { get; set; }
        public City city { get; set; }
        public int cnt { get; set; }
        public List<Forecast> list { get; set; }
    }

    public class City
    {
        public string id { get; set; }
        public string name { get; set; }
        public Coord coord { get; set; }
    }

    public class Coord
    {
        public string lat { get; set; }
        public string lon { get; set; }
    }

    public class Forecast
    {
        public string dt { get; set; }
        public ForecastMain main { get; set; }
        public List<WeatherMoreInfo> weather { get; set; }
        public string dt_txt { get; set; }
    }

    public class ForecastMain
    {
        public string temp { get; set; }
        public string temp_min { get; set; }
        public string temp_max { get; set; }
        public string pressure { get; set; }
        public string sea_level { get; set; }
        public string grnd_level { get; set; }
        public string humidity { get; set; }
        public string temp_kf { get; set; }
    }

    public class WeatherMoreInfo
    {
        public string id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }
}
