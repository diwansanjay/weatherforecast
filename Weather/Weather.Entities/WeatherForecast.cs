﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Entities
{
    public class WeatherForecast
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Temparature> temparature { get;set;}
    }

    public class Temparature
    {
        public string dt { get; set; }
        public decimal minTemp { get; set; }
        public decimal maxTemp { get; set; }
        public decimal temp { get; set; }
        public string description { get; set; }
        public string tempImageUrl { get; set; }
    }
}
