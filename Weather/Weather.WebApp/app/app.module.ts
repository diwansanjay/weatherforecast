import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule }       from '@angular/common';
import { FormsModule }        from '@angular/forms';
import {WeatherComponent} from './weather.component';
import {HttpModule} from '@angular/http';
import {WeatherService } from './weather.service';

@NgModule({
  imports:      [ BrowserModule, CommonModule, FormsModule,HttpModule],
  declarations: [WeatherComponent],
  providers:[WeatherService],
  exports:[],
  bootstrap: [WeatherComponent ]
})
export class AppModule { }