import {Component, OnInit,  Input, Output, EventEmitter} from '@angular/core';
import {WeatherService} from './weather.service';
import {Http, Response} from '@angular/http';

/* Adding main app component */
@Component({
  selector: 'main-app',
  templateUrl: 'app/weather.tpl.html'
})

export class WeatherComponent{
    title: string = "Weather Forecast component";
    public weatherForecast: weatherForecast;
    constructor(public weatherService: WeatherService) {
    };

    public getWeatherForecast(cityName: string) {
        this.weatherService.getWeatherForecast(cityName).subscribe((c) => {
            this.weatherForecast = c.json();
        });
    }
}

interface weatherForecast
{
    id: string;
    name: string;
    temparature: temparature[];
}

interface temparature
{
    minTemp: string;
    dt: string;
    maxTemp: string;
    temp: string;
    tempImageUrl: string;
    description: string;
}