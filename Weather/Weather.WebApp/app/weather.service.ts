﻿import {Injectable} from '@angular/core'
import {Http} from '@angular/http'
import {Observable} from 'rxjs/Observable'

@Injectable()
export class WeatherService {
    _http: Http;
    private _url = "http://localhost:58900/weather/";
    constructor(http: Http) {
        this._http = http;
    }
    getWeatherForecast(cityName:string) {
        return this._http.get(this._url + cityName);
    }
}
