﻿import {Component} from '@angular/core';

import 'rxjs/Rx';

@Component({
    selector: 'root-app',
    template: `
    <span style="text-align:center">
    <h1>Hello World :{{anyModelName }}</h1></span> `
})
export class RootComponent {
    public anyModelName = "Edureka - AngularJS Introduction.";
}